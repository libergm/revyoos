function sliderChange(val) {
  const x = val;
  var franja = parseInt(500/11);

  if (x < (franja*7)) {
    document.getElementById('card-premium').style.opacity = "1";
    document.getElementById('card-premium').style.filter  = 'alpha(opacity=100)';
    document.getElementById('premium-btn').style.visibility  = 'visible';
  } else {
    document.getElementById('card-premium').style.opacity = "0.5";
    document.getElementById('card-premium').style.filter  = 'alpha(opacity=50)';
    document.getElementById('premium-btn').style.visibility  = 'hidden';
  }

  switch (true) {
      case (x < 2):
          document.getElementById('output').innerHTML = "1";
          document.getElementById('price-premium-month').innerHTML = "5";
          document.getElementById('price-premium-property').innerHTML = "5";
          document.getElementById('price-business-month').innerHTML = "10";
          document.getElementById('price-business-property').innerHTML = "10";
          document.getElementById('price-premium-year').innerHTML = "45";
          document.getElementById('price-business-year').innerHTML = "90";
          break;
      case (x < (franja)):
          document.getElementById('output').innerHTML = "2 - 4";
          document.getElementById('price-premium-month').innerHTML = "7";
          document.getElementById('price-premium-property').innerHTML = "3,50";
          document.getElementById('price-business-month').innerHTML = "12";
          document.getElementById('price-business-property').innerHTML = "6";
          document.getElementById('price-premium-year').innerHTML = "63";
          document.getElementById('price-business-year').innerHTML = "108";
          break;
      case (x < (franja*2)):
          document.getElementById('output').innerHTML = "5 - 9";
          document.getElementById('price-premium-month').innerHTML = "11";
          document.getElementById('price-premium-property').innerHTML = "2,20";
          document.getElementById('price-business-month').innerHTML = "18";
          document.getElementById('price-business-property').innerHTML = "3,60";
          document.getElementById('price-premium-year').innerHTML = "99";
          document.getElementById('price-business-year').innerHTML = "162";
          break;
      case (x < (franja*3)):
          document.getElementById('output').innerHTML = "10 - 19";
          document.getElementById('price-premium-month').innerHTML = "19";
          document.getElementById('price-premium-property').innerHTML = "1,90";
          document.getElementById('price-business-month').innerHTML = "29";
          document.getElementById('price-business-property').innerHTML = "2,90";
          document.getElementById('price-premium-year').innerHTML = "171";
          document.getElementById('price-business-year').innerHTML = "261";
          break;
      case (x < (franja*4)):
          document.getElementById('output').innerHTML = "20 - 29";
          document.getElementById('price-premium-month').innerHTML = "25";
          document.getElementById('price-premium-property').innerHTML = "1,25";
          document.getElementById('price-business-month').innerHTML = "39";
          document.getElementById('price-business-property').innerHTML = "1,95";
          document.getElementById('price-premium-year').innerHTML = "225";
          document.getElementById('price-business-year').innerHTML = "351";
          break;
      case (x < (franja*5)):
          document.getElementById('output').innerHTML = "30 - 49";
          document.getElementById('price-premium-month').innerHTML = "39";
          document.getElementById('price-premium-property').innerHTML = "1,30";
          document.getElementById('price-business-month').innerHTML = "59";
          document.getElementById('price-business-property').innerHTML = "1,97";
          document.getElementById('price-premium-year').innerHTML = "351";
          document.getElementById('price-business-year').innerHTML = "531";
          break;
      case (x < (franja*6)):
          document.getElementById('output').innerHTML = "50 - 69";
          document.getElementById('price-premium-month').innerHTML = "49";
          document.getElementById('price-premium-property').innerHTML = "0,98";
          document.getElementById('price-business-month').innerHTML = "69";
          document.getElementById('price-business-property').innerHTML = "1,38";
          document.getElementById('price-premium-year').innerHTML = "441";
          document.getElementById('price-business-year').innerHTML = "621";
          break;
      case (x < (franja*7)):
          document.getElementById('output').innerHTML = "70 - 99";
          document.getElementById('price-premium-month').innerHTML = "55";
          document.getElementById('price-premium-property').innerHTML = "0,79";
          document.getElementById('price-business-month').innerHTML = "77";
          document.getElementById('price-business-property').innerHTML = "1,10";
          document.getElementById('price-premium-year').innerHTML = "495";
          document.getElementById('price-business-year').innerHTML = "693";
          break;
      case (x < (franja*8)):
          document.getElementById('output').innerHTML = "100 - 149";
          document.getElementById('price-premium-month').innerHTML = "-";
          document.getElementById('price-premium-property').innerHTML = "-";
          document.getElementById('price-business-month').innerHTML = "95";
          document.getElementById('price-business-property').innerHTML = "0,95";
          document.getElementById('price-premium-year').innerHTML = "-";
          document.getElementById('price-business-year').innerHTML = "855";
          break;
      case (x < (franja*9)):
          document.getElementById('output').innerHTML = "150 - 199";
          document.getElementById('price-premium-month').innerHTML = "-";
          document.getElementById('price-premium-property').innerHTML = "-";
          document.getElementById('price-business-month').innerHTML = "105";
          document.getElementById('price-business-property').innerHTML = "0,70";
          document.getElementById('price-premium-year').innerHTML = "-";
          document.getElementById('price-business-year').innerHTML = "945";
          break;
      case (x < (franja*10)):
          document.getElementById('output').innerHTML = "200 - 299";
          document.getElementById('price-premium-month').innerHTML = "-";
          document.getElementById('price-premium-property').innerHTML = "-";
          document.getElementById('price-business-month').innerHTML = "125";
          document.getElementById('price-business-property').innerHTML = "0,63";
          document.getElementById('price-premium-year').innerHTML = "-";
          document.getElementById('price-business-year').innerHTML = "1.125";
          break;
      case (x < 500):
          document.getElementById('output').innerHTML = "300 - 500";
          document.getElementById('price-premium-month').innerHTML = "-";
          document.getElementById('price-premium-property').innerHTML = "-";
          document.getElementById('price-business-month').innerHTML = "165";
          document.getElementById('price-business-property').innerHTML = "0,55";
          document.getElementById('price-premium-year').innerHTML = "-";
          document.getElementById('price-business-year').innerHTML = "1.485";
          break;
      default:
          document.getElementById('output').innerHTML = "300 - 500";
          document.getElementById('price-premium-month').innerHTML = "-";
          document.getElementById('price-premium-property').innerHTML = "-";
          document.getElementById('price-business-month').innerHTML = "165";
          document.getElementById('price-business-property').innerHTML = "0,55";
          document.getElementById('price-premium-year').innerHTML = "-";
          document.getElementById('price-business-year').innerHTML = "1.485";
          break;
  }
}
document.getElementById('slider').value = 250;
document.getElementById('output').innerHTML = "50 - 69";
document.getElementById('price-premium-month').innerHTML = "49";
document.getElementById('price-premium-property').innerHTML = "0,98";
document.getElementById('price-business-month').innerHTML = "69";
document.getElementById('price-business-property').innerHTML = "1,38";
document.getElementById('price-premium-year').innerHTML = "441";
document.getElementById('price-business-year').innerHTML = "621";
